

public class Time {
    //часы и минуты
    private int hours, minutes;

    public Time(int h, int m) {
        this.hours = h;
        this.minutes = m;
    }

    //мы можем представлять время как, например, 2,5 часа
    public Time(double dec) {
        this();
        fromDecimalForm(dec);
    }

    //а можем и вовсе создавать время из строки "12:23"
    public Time(String s) {
        String[] parts = s.split(":");
        this.hours = Integer.parseInt(parts[0]);
        this.minutes = Integer.parseInt(parts[1]);
    }

    public Time() {
        this(0, 0);
    }

    /**
     * @return час
     */
    public int getHours() {
        return this.hours;
    }

    /**
     * @return минуты
     */
    public int getMinutes() {
        return this.minutes;
    }

    /**
     * @return десятичная форма времени
     */
    public double toDecimalForm() {
        return hours + (minutes / 60.0);
    }

    /**
     * @param dec десятичная форма времени
     */
    public void fromDecimalForm(double dec) {
        this.hours = (int)Math.floor(dec);
        this.minutes = (int)(dec * 60) % 60;
    }

    /**
     * Складывает время. К примеру, 12:00 + 2:30 = 14:30
     *
     * @param other время-слагаемое
     * @return сумма временных отрезков
     */
    public Time plus(Time other) {
        double dec = this.toDecimalForm() + other.toDecimalForm();
        return new Time(dec);
    }

    /**
     * Вычитает время. К примеру, 12:00 - 2:30 = 9:30
     *
     * @param other время-вычитаемое
     * @return разность временных отрезков
     */
    public Time minus(Time other) {
        double dec = this.toDecimalForm() - other.toDecimalForm();
        return new Time(dec);
    }

    /**
     * Сокращает временной отрезок в c раз
     * К примеру, 12:00 / 3 = 4:00
     *
     * @param c делитель
     * @return временной отрезок уменьшенный в c раз
     */
    public Time divide(int c) {
        return new Time(this.toDecimalForm() / c);
    }

    /**
     * Увеличивает временной отрезок в c раз
     * К примеру, 4:00 * 3 = 12:00
     *
     * @param c множитель
     * @return временной отрезок увеличенный в c раз
     */
    public Time multiple(int c) {
        return new Time(this.toDecimalForm() * c);
    }

    /**
     * @param other временной отрезок
     * @return true, если данный отрезок меньше other
     */
    public boolean isLessThan(Time other) {
        return Time.min(this, other).equals(this);
    }

    /**
     * @param other временной отрезок
     * @return true, если данный отрезок болньше other
     */
    public boolean isBiggerThan(Time other) {
        return Time.max(this, other).equals(this);
    }

    public String toString() {
        StringBuilder b = new StringBuilder();

        //если часов меньше десяти, то приписываем с начала 0 для красоты (09, 03)
        if(hours < 10)
            b.append(0);
        //припимываем часы и двоеточие
        b.append(hours).append(":");

        //если минут меньше десяти, то приписываем с начала 0 для красоты (09, 03)
        if(minutes < 10)
            b.append(0);
        //приписываем минуты и двоеточие
        b.append(minutes);

        //время в формате ЧЧ:ММ, к примеру 01:37
        return b.toString();
    }

    /**
     * @param a время
     * @param b время
     * @return большее из двух временных отрезок
     */
    public static Time max(Time a, Time b) {
        if(a.toDecimalForm() > b.toDecimalForm())
            return a;
        return b;
    }

    /**
     * @param a время
     * @param b время
     * @return меньшее из двух временных отрезок
     */
    public static Time min(Time a, Time b) {
        if(a.toDecimalForm() < b.toDecimalForm())
            return a;
        return b;
    }
}
